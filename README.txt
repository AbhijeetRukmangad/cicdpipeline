
Infostretch Corporation Pvt. Ltd.

Project Goal:
  Create a android assembly line to continuosly integrate and deliver stable releases with maximum automation.

Strutcure of repository
  [root] 
    |
    | - docs : contains all documents for help/reference
    | - scripts : contains all scripts written so far
    | - readme.txt

Intended tools to be used 
 - Jenkins
 - Pipeline plugin for Jenkins
 - Artifcatory
 - Docker
 - Android SDK & Framework
 - Gradle
 - Static code analyzers
 - Google Publishing API
 - Appium 
