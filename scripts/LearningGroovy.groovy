
/* --------------------------------------------------------------------------------
      Function call
   --------------------------------------------------------------------------------*/
println "Hello World!"

println (checkme(0));

println (checkme(1));


boolean checkme(int val) {
	if (val == 0) {
		return true;
	} else {
		return false;
	}
}

/* --------------------------------------------------------------------------------
      Verify global variables
   --------------------------------------------------------------------------------*/


TARGET_EMULATOR = 0;

BUILD_TYPE = TARGET_EMULATOR

node {


//doSomething()

echo 'start'



}// End of node



def doSomething() {
    
    echo "target ${TARGET_EMULATOR}"
    
    input message: 'asdfsdfsadf', parameters: [[$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'BUILD_ALLOWME']]
    
    echo "received input as ${env.BUILD_ALLOWME}"

    echo " build type is ${BUILD_TYPE} "
    if (BUILD_TYPE == 0){
        echo 'case1'
        echo BUILD_TYPE
    } else if (BUILD_TYPE == TARGET_EMULATOR){
        echo 'case2'
        echo BUILD_TYPE
    } 
}

/* --------------------------------------------------------------------------------
          Running external jobs and sending hipchat message
   --------------------------------------------------------------------------------*/

node {
	echo "start"

	build job: 'temp', parameters: [[$class: 'StringParameterValue', name: 'directory', value: pwd()]]

	build job: 'temp', parameters: [[$class: 'StringParameterValue', name: 'sourcedir', value: 'D:\\cicd\\AndroidProject\\CICDAppliaction\\app\\src\\main\\java'], [$class: 'StringParameterValue', name: 'resultfile', value: 'D:\\cicd\\CICDPipeline\\checkstyle_results.xml']]

	hipchatSend color: 'YELLOW', failOnError: true, message: 'THIS MESSAGE IS FROM JENKINS', room: 'infostretch-jenkins', sendAs: 'Jenkins Team', server: 'infostretch-jenkins.hipchat.com', token: 'e06f8ca070252d633d354b1e97865f', v2enabled: true

	echo "end"

}

/* --------------------------------------------------------------------------------
        catchError block
   --------------------------------------------------------------------------------*/
node {
  echo "start"
  
  catchError {
    echo "before doing something"
    doSomething();
    echo "after doing something"
  } 
  
  echo "after catch block"

}

def doSomething() {
    //try {
        echo "inside doSomething"
        //throw new Exception()
       //error 'error while doing something'
       bat "abrakadabra"

        echo "returning from doSomething"
    //} catch(err) {
     //   echo "inside catch block of doSomething"
     //   currentBuild.result = "Failure"
    //}
}


/* --------------------------------------------------------------------------------
          Running Checkstyle as a external tool
   --------------------------------------------------------------------------------*/

/*

    String javafiles = "${pwd()}\\app\\src\\main\\java"
    String checkstyleresultfile = "${pwd()}\\app\\build\\reports\\checkstyle\\checkstyle.xml"
    
    build job: 'temp',
            parameters: [
             [$class: 'StringParameterValue',
             name: 'sourcedir',
             value: javafiles],
             [$class: 'StringParameterValue',
             name: 'resultfile',
             value: checkstyleresultfile
             ]
            ]
    */