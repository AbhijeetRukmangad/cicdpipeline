

node {

    
    catchError {

        /*
        Checkout: this stage is resposible for pulling the source code from SCM to workspace.
        */
        stage 'Stage 1'
        stage1('*.txt','tmp.txt')
        echo 'Stage 1 success'

    }

}// End of node


def stage1(includes, targetfilename) {
    
    echo "Archiving11"
    
    def server = Artifactory.server('CICDArtifactory')
    
    echo "includes : ${includes}"
    
    echo "target: ${env.BUILD_TAG+'_'+targetfilename}"

    def uploadSpec = "{\"files\": [{\"pattern\": \"${includes}\", \"target\": \"android_pipeline/${env.BUILD_TAG+'_'+targetfilename}\"}]}"

    server.upload(uploadSpec)
        
    
}


/*
    ===============================================================================================================================================
    ===============================================================================================================================================
*/


AUTOMATION_UNIT_TEST_RESULTS_PATH = "test/target/surefire-reports/testng-junit-results"
AUTOMATION_NATIVE_TEST_RESULTS_PATH = "test/target/surefire-reports/testng-native-results"

node {
        
        stage 'Stage 1'
        stage1()
        echo 'Stage 1 success'

}// End of node


def stage1() {
    
    echo "publishing unit test report"

    echo pwd()
    
    publishHTML([allowMissing: true, 
        alwaysLinkToLastBuild: false, 
        keepAll: false, 
        reportDir: "${AUTOMATION_UNIT_TEST_RESULTS_PATH}", 
        reportFiles: 'index.html', 
        reportName: 'Automation unittest report'
    ])
    
    echo "publishing unit test report"

    publishHTML([allowMissing: true, 
        alwaysLinkToLastBuild: false, 
        keepAll: false, 
        reportDir: "${AUTOMATION_NATIVE_TEST_RESULTS_PATH}", 
        reportFiles: 'index.html', 
        reportName: 'Automation nativetest report'
    ])
        
    
}


/*
    ===============================================================================================================================================
    ===============================================================================================================================================
*/


CHECKSTYLE_CLASSPATH = "D:\\cicd\\Installables\\checkstyle-6.18.jar"
CHECKSTYLE_CONFIG_FILE = "D:\\cicd\\Installables\\checkstyle.xml"
CHECKSTYLE_FORMAT = "xml"
CHECKSTYLE_RESULT_FILE = "checkstyleresults.xml"
SOURCE_DIR = "dev/app/src/main/java"

node {
    stage : "stage1"
    echo "dir ${pwd()}"
    catchError {
        stage1()
    }
    stage2()
    echo "Success"
}


def stage1() {
    String command = "java -jar ${CHECKSTYLE_CLASSPATH} -c ${CHECKSTYLE_CONFIG_FILE} -f ${CHECKSTYLE_FORMAT} -o ${CHECKSTYLE_RESULT_FILE} ${SOURCE_DIR}" 
    bat command
}

def stage2() {
    step([
        $class: 'CheckStylePublisher',
         canComputeNew: false,
         defaultEncoding: '',
         canRunOnFailed: true,
         healthy: '',
         pattern: '**/checkstyleresults.xml',
         unHealthy: ''
    ])
}



